$(function () {
    'use strict';
    let pushMenu = $('[data-toggle="push-menu"]');
    pushMenu.pushMenu();
    let $pushMenu = pushMenu.data('lte.pushmenu');
    $(window).on('load', function() {
        // Reinitialize variables on load
        $pushMenu = pushMenu.data('lte.pushmenu');
    });

    $('[data-enable="expandOnHover"]').on('click', function () {
        $(this).attr('disabled', true);
        $pushMenu.expandOnHover();
        if (!$('body').hasClass('sidebar-collapse'))
            $('[data-layout="sidebar-collapse"]').click()
    });
});
